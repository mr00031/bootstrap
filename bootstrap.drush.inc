<?php
/**
 * @file
 * Drupal Bootstrap Drush commands.
 */
use Drupal\bootstrap\Bootstrap;
use Drupal\bootstrap\Theme;
use Drupal\Component\Serialization\Yaml;

/**
 * Implements hook_drush_command().
 */
function bootstrap_drush_command() {
  $items = array();
  $items['bootstrap-generate-docs'] = [
    'description' => dt('Generates markdown documentation for the Drupal based code.'),
    'arguments' => [
      'type' => 'The specific type of documentation to generate, defaults to "all". Can be: "all", "settings".',
    ],
    'aliases' => ['bs-docs'],
  ];
  $items['bootstrap-sub-theme'] = array(
    'description' => 'Create a Bootstrap foundation sub-theme',
    'aliases' => array('bsass'),
    'arguments' => array(
      'name'         => 'Your sub-theme name.',
      'machine_name' => 'A machine-readable name for your theme, optional only  [a-z, 0-9] ',
    ),
    'options' => array(
      'description'  => 'Your sub-theme description.',
      'machine-name' => '[a-z, 0-9] A machine-readable name for your theme.'
    ),
    'examples' => array(
      'drush bsass "custom theme name"' => 'Create a sub-theme with the default options.',
      'drush bsass "foo bar" "foo_bar"  --description="My supersweet awesome theme"' => 'Create a sub-theme with additional options.',
    ),
  );

  return $items;
}

/**
 * Generates markdown documentation.
 *
 * @param string $type
 */
function drush_bootstrap_generate_docs($type = 'all') {
  $types = $type === 'all' ? ['settings'] : [$type];
  foreach ($types as $type) {
    $function = "_drush_bootstrap_generate_docs_$type";
    if (function_exists($function)) {
      $ret = $function(Bootstrap::getTheme('bootstrap'));
      if ($ret) {
        drush_log('Successfully generated documentation for: ' . $type, 'success');
      }
      else {
        drush_log('Unable to generate documentation for: ' . $type, 'error');
      }
    }
    else {
      drush_log('Invalid documentation type: ' . $type, 'error');
    }
  }
}

/**
 * Generates settings documentation.
 *
 * @param \Drupal\bootstrap\Theme $bootstrap
 *   The theme instance of the Drupal Bootstrap base theme.
 */
function _drush_bootstrap_generate_docs_settings(Theme $bootstrap) {
  $output[] = '<!-- @file Overview of theme settings for Drupal Bootstrap based themes. -->';
  $output[] = '<!-- @defgroup -->';
  $output[] = '<!-- @ingroup -->';
  $output[] = '# Theme Settings';
  $output[] = '';
  $output[] = 'To override a setting, open `./config/install/THEMENAME.settings.yml` and add the following:';
  $output[] = '';
  $output[] = '```yaml';
  $output[] = '# Settings';
  $output[] = '';
  $output[] = 'settings:';
  $output[] = '  SETTING_NAME: SETTING_VALUE';
  $output[] = '```';

  // Determine the groups.
  $groups = [];
  foreach ($bootstrap->getSettingPlugin() as $setting) {
    // Only get the first two groups (we don't need 3rd, or more, levels).
    $_groups = array_slice($setting->getGroups(), 0, 2, FALSE);
    if (!$_groups) {
      continue;
    }
    $groups[implode(' > ', $_groups)][] = $setting->getPluginDefinition();
  }

  // Generate a table of each group's settings.
  foreach ($groups as $group => $settings) {
    $output[] = '';
    $output[] = '---';
    $output[] = '';
    $output[] = "### $group";
    $output[] = '';
    $output[] = '<table class="table table-striped table-responsive">';
    $output[] = '  <thead><tr><th class="col-xs-3">Setting name</th><th>Description and default value</th></tr></thead>';
    $output[] = '  <tbody>';
    foreach ($settings as $definition) {
      $output[] = '  <tr>';
      $output[] = '    <td class="col-xs-3">' . $definition['id'] . '</td>';
      $output[] = '    <td>';
      $output[] = '      <div class="help-block">' . str_replace('&quote;', '"', $definition['description']) . '</div>';
      $output[] = '      <pre class=" language-yaml"><code>' . Yaml::encode([$definition['id'] => $definition['defaultValue']]) . '</code></pre>';
      $output[] = '    </td>';
      $output[] = '  </tr>';
    }
    $output[] = '  </tbody>';
    $output[] = '</table>';
  }

  // Ensure we have link references at the bottom.
  $output[] = '';
  $output[] = '[Drupal Bootstrap]: https://www.drupal.org/project/bootstrap';
  $output[] = '[Bootstrap Framework]: http://getbootstrap.com';

  // Save the generated output to the appropriate file.
  return file_put_contents(realpath($bootstrap->getPath() . '/docs/Theme-Settings.md'), implode("\n", $output)) !== FALSE;
}

/**
 * Create a Boostrap sub-theme.
 */
function drush_bootstrap_sub_theme($name = NULL, $machine_name = NULL, $description = NULL) {
  if (empty($name)) {
    drush_set_error(dt("Please provide a name for the sub-theme.\nUSAGE:\tdrush bsass [name] [machine_name !OPTIONAL] [description !OPTIONAL]\n"));
    return;
  }
  //Filter everything but letters, numbers, underscores, and hyphens
  $machine_name = !empty($machine_name) ? preg_replace('/[^a-z0-9_-]+/', '', strtolower($machine_name)) : preg_replace('/[^a-z0-9_-]+/', '', strtolower($name));
  // Eliminate hyphens
  $machine_name = str_replace('-', '_', $machine_name);

  $bootstrap_path = drush_get_context('DRUSH_DRUPAL_ROOT') . '/' . drupal_get_path('theme', 'bootstrap');
  $subtheme_path = explode('/', $bootstrap_path);
  array_pop($subtheme_path);
  $subtheme_path = implode('/', $subtheme_path) . '/' . $machine_name;

  // Make a fresh copy of the subtheme.
  $s = drush_copy_dir("$bootstrap_path/starterkits/sass", $subtheme_path);
  if (empty($s)) {
    return;
  }

  // Rename files and fill in the theme machine name
  drush_op('rename', "$subtheme_path/THEMENAME.starterkit.yml", "$subtheme_path/$machine_name.info.yml");
  drush_op('rename', "$subtheme_path/THEMENAME.libraries.yml", "$subtheme_path/$machine_name.libraries.yml");
  drush_op('rename', "$subtheme_path/THEMENAME.theme", "$subtheme_path/$machine_name.theme");
  drush_op('rename', "$subtheme_path/config/install/THEMENAME.settings.yml", "$subtheme_path/config/install/$machine_name.settings.yml");
  drush_op('rename', "$subtheme_path/config/schema/THEMENAME.schema.yml", "$subtheme_path/config/schema/$machine_name.schema.yml");
  drush_op('rename', "$subtheme_path/js/THEMENAME.app.js", "$subtheme_path/js/$machine_name.app.js");
  drush_op('rename', "$subtheme_path/scss/THEMENAME.style.scss", "$subtheme_path/scss/$machine_name.style.scss");

 // Rename Config Optional Blocks
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_account_menu.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "account_menu.yml");
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_branding.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "branding.yml");
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_breadcrumbs.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "breadcrumbs.yml");
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_content.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "content.yml");
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_footer.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "footer.yml");
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_help.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "help.yml");
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_local_actions.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "local_actions.yml");
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_local_tasks.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "local_tasks.yml");
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_main_menu.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "main_menu.yml");
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_messages.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "messages.yml");
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_page_title.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "page_title.yml");
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_powered.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "powered.yml");
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_search.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "search.yml");
  drush_op('rename', "$subtheme_path/config/optional/block.block.bootstrap_tools.yml", "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "tools.yml");

  // Change Optional Bootstrap Blocks to the name of the theme
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "account_menu.yml", 'bootstrap', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "branding.yml", 'bootstrap', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "breadcrumbs.yml", 'bootstrap', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "content.yml", 'bootstrap', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "footer.yml", 'bootstrap', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "help.yml", 'bootstrap', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "local_actions.yml", 'bootstrap', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "local_tasks.yml", 'bootstrap', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "main_menu.yml", 'bootstrap', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "messages.yml", 'bootstrap', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "page_title.yml", 'bootstrap', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "powered.yml", 'bootstrap', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "search.yml", 'bootstrap', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/optional/block.block.$machine_name" . '_' . "tools.yml", 'bootstrap', "$machine_name");

  // Change the name of the theme.
  drush_op('bootstrap_file_str_replace', "$subtheme_path/$machine_name.info.yml", 'THEMETITLE', "$name");

  // Change the name of the theme.
  if (!empty($description)) {
    drush_op('bootstrap_file_str_replace', "$subtheme_path/$machine_name.info.yml", 'Custom sub-theme, inherits from the Bootstrap base theme', $description);
  }

  // Replaces instances of THEMENAME in required files to name of the theme.
  drush_op('bootstrap_file_str_replace', "$subtheme_path/$machine_name.info.yml", 'THEMENAME', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/$machine_name.libraries.yml", 'THEMENAME', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/gulpfile.js", 'THEMENAME', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/package.json", 'THEMENAME', "$machine_name");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/js/$machine_name.app.js", 'THEMENAME', "$machine_name");

  // Replaces instances of THEMENAME in required files to name of the theme.
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/install/$machine_name.settings.yml", 'THEMENAME.settings', "$machine_name" . "." . "settings");
  drush_op('bootstrap_file_str_replace', "$subtheme_path/config/schema/$machine_name.schema.yml", 'THEMETITLE', "$name");

  // Notify user of the newly created theme.
  drush_print(dt("\n!name sub-theme was created in !path. \n",
    array(
      '!name' => $name,
      '!path' => $subtheme_path,
    )
  ));
  drush_pm_enable_validate($machine_name);
  drush_pm_enable($machine_name);

}

/**
 * Internal helper: Replace strings in a file.
 */
function bootstrap_file_str_replace($file_path, $find, $replace) {
  $file_contents = file_get_contents($file_path);
  $file_contents = str_replace($find, $replace, $file_contents);
  file_put_contents($file_path, $file_contents);
}

/**
 * Implements hook_drush_help().
 */
function bootstrap_drush_help($section) {
  switch ($section) {
    case 'drush:bootstrap-sub-theme':
      return dt("Create a Boostrap custom sub-theme.");
  }
}